### Arduino permissions

### Get USB device List
```
lsusb -vvv
```


### Commands to test permissions
```
#AS IDE
avrdude -v -p m2560 -c usbtiny

#AS ROOT
sudo avrdude -v -p m2560 -c usbtiny
```


### Example File
```
#sudo vi  /etc/udev/rules.d/50-myusb.rule

SUBSYSTEMS=="usb", ATTRS{idVendor}=="0x1781", ATTRS{idProduct}=="0x0c9f", GROUP="test", MODE="0777"
```

### Reload permissions
```
sudo udevadm control --reload
```


